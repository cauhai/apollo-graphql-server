import locallydb from 'locallydb';
import axios from 'axios';

const db = new locallydb('./data');

/* testing 
(function () {
  axios.get('https://wttr.in/Moscow?format=j1').then(function(response) {
    console.log('Moscow', response.data.current_condition);
  })
}())*/

// example of retrieve from database 
const getAllCitiesFromDB = () => db.collection('cities').items;

// example of making REST API


/*phonetic_OK: async (parent, args, contextValue, info) => {
  return new Promise((resolve, reject) => {
    axios.get(`https://api.dictionaryapi.dev/api/v2/entries/en/${args.word}`)
    .then(resp => {
      const result = resp.data[0] // why [0]? 
      return resolve({
        word: result.word,
        phonetic: result.phonetic,
      })
    })
  })
},*/

export const resolverMap = {
  Query: {
    cities: getAllCitiesFromDB,

    phonetic: async (parent, args, contextValue, info) => {
      try {
        const resp = await axios.get(`https://api.dictionaryapi.dev/api/v2/entries/en/${args.word}`)
        const result = resp.data[0] // why [0]? Because that's what response looks like
        return {
          word: result.word,
          phonetic: result.phonetic,
        }
      } catch (error) {
        throw new Error(error.response.data.message || 'Cannot find what you want!')
      }
    }
  },
};