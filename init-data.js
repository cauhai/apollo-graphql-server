import locallydb from 'locallydb';
import fs from 'fs';

fs.unlinkSync('./data/cities');
const db = new locallydb('./data');
const cities = db.collection('cities');
[
  { name: 'Paris', countryCode: 'fr'},
  { name: 'Krakow', countryCode: 'pl'},
  { name: 'Sofia', countryCode: 'bg' },
  { name: 'Venice', countryCode: 'it' },
  { name: 'Hong Kong', countryCode: 'cn' },
  { name: 'Edmonton', countryCode: 'ca' },
].forEach(city => {
    cities.insert(city);
});
cities.save();

console.log('Initial data for cities', cities.items)