Making a graphql server with Apollo
https://www.apollographql.com/docs/apollo-server/getting-started

Some data are stored in database (using local files, by http://boutglay.com/locallydb/)
Some data are obtained from REST API, such as https://dictionaryapi.dev

## Developer setup

Clone, then run `npm install`

Run this one time to get a starting set of data
`npm run init-data`

To start server:
`npm start`

and visit `http://localhost:4000/`
then play with queries


## Materials
https://www.apollographql.com/docs/apollo-server
https://dictionaryapi.dev
https://api.dictionaryapi.dev/api/v2/entries/en/
