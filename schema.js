export const gqlSchema = `#graphql
  
  type City {
    name: String!
    countryCode: String! # two-letter code (ISO Alpha-2)
  }


  type WordPhonetic {
    word: String
    phonetic: String!
  }

  type Query {
    cities: [City]
    phonetic(word: String!): WordPhonetic
  }








  interface MutationResponse {
    code: String!
    success: Boolean!
    message: String!
  }

  type AddCityMutationResponse implements MutationResponse {
    code: String!
    success: Boolean!
    message: String!
    city: City
  }
  type Mutation {
    addCity(name: String!, countryCode: String!): City
  }
`;